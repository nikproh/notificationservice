package notificationservice.client;


import java.net.Socket;


/** Class with main method to send command to server.
*/
public class TCPClient {  
    /** Server's port. It is hardcoded, but maybe it is better to read it
     * from external file with ResourceBundle for example. */
    private static final int port = 3137;
    
    /** Connect to server and sent given command.    
     * @param command - given command
    */
    private static void commandSend(String command) {
        try (Socket socket = new Socket("localhost", port)) {           
            socket.getOutputStream().write(command.getBytes());            
        } catch (Exception e) { 
            System.out.println("Client socket error: " + e);
        }
    }
    
    /** Main method to send 3 test command to server.
     * First command - mail notification.
     * Second command - http notification.
     * Third command - command with wrong syntax.
    */
    public static void main(String args[]) {        
        String command1 = "id1,Hello!,13:01 13.09.2016,mail,email=nikproh@inbox.ru";
        String command2 = "id2,Mozilla/5.0,12:50 13.09.2016,http,url=http://google.ru";
        String command3 = "id3ERROR,12:32 13.09.2016,http,something";                 
        commandSend(command1);
        commandSend(command2);
        commandSend(command3);
    }
}
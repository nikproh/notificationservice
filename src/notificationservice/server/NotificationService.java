package notificationservice.server;


/** Class with main method to start the server instance.
*/
public class NotificationService {    
    public static void main(String[] args) {
        new TCPServer().start(); 
    }
}

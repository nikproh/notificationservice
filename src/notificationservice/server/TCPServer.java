package notificationservice.server;


import java.net.InetAddress;
import java.net.ServerSocket;


/** Class stores a server object.
*/
public class TCPServer {
    /** Server's port. It is hardcoded, but maybe it is better to read it
     * from external file with ResourceBundle for example. */
    private final int port = 3137;
    
    /** Start the server instance. When command is received new thread is 
     * started to deal with new socket.
     * @see ConnectionThread
    */
    public void start() {
        try (ServerSocket server = 
                new ServerSocket(port, 0, InetAddress.getByName("localhost"))) {
            System.out.println("Server is started");        
            while (true) {            
                new ConnectionThread(server.accept()).start();
            }
        } catch (Exception e) { 
            System.out.println("Server socket error: " + e);
        }
    }
}

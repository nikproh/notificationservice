package notificationservice.server;


/** Class has get metod that create runnable object for given notification.
*/
public class NotificationRunnable {
    /** Method check given notification <b>notification_type</> field and 
     * create corresponding runnable object.
     * @param notification - given notification
    */
    static Runnable get(Notification notification) {        
        switch (notification.getNotification_type()) {
        case http:  
            return new HTTPNotification(notification);
        case mail:  
            return new SMTPNotification(notification);    
        }
        return null;
    }
}

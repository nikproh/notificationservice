package notificationservice.server;


import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;


/** Runnable for Notification object with NotificationType <b>mail</b>. */
public class SMTPNotification implements Runnable {
    /** Fields for connection to smtp server. It is hardcoded, but maybe it is 
     * better to read it from external file with ResourceBundle for example. 
     */
    private final String username = "nikprofortest@gmail.com";
    private final String email = username;
    private final String password = "qwerty##";
    private final String subject = "notification";    
    private final String body;
    private final String recepient;
    /** Pattern for parse of extra_params field of Notification object. */
    private static final Pattern pattern = Pattern.compile("email=(.+)");  
    
    /** Create an object with given notification. */
    SMTPNotification(Notification notification) {       
        body = notification.getMessage();
        Matcher m = pattern.matcher(notification.getExtra_params());
        if (m.matches()) {
            recepient = m.group(1);
        } else {
            recepient = "";
        }
    }
    
    /** Send mail using javax.mail library. In current version <b>gmail</b>
     * smtp server is used.
    */
    public void sendMail() { 
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");
        Authenticator authenticator = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(username, password);
            }
        };
        Session session = Session.getInstance(props, authenticator);
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(recepient));
            message.setSubject(subject);
            message.setText(body);
            Transport.send(message);
            System.out.println("Email was send");
        } catch (MessagingException ex) {
            System.out.println("Error while sending email: " + ex);
        }
    }
    
    /** Calls sendMail method.
     * @see sendMail()
    */
    @Override
    public void run() {  
        if (!recepient.equals("")) {
            sendMail();
        }
    }
}

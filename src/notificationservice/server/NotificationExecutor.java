package notificationservice.server;


import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


/** Class execute notifications.
*/
public class NotificationExecutor {
    /** Scheduled executor that is used to execute notifications.
     * 
    */    
    private static final ScheduledExecutorService executor = 
            Executors.newScheduledThreadPool(2); 
    
    /** Add given Runnable object to schedule for execute at given time (this 
     * happen only if given time not in the past). 
     * @param run - given Runnable object
     * @param date - given time
    */ 
    public static void addNotification(Runnable run, Date date) {
        long delay = date.getTime() - new Date().getTime();
        if (delay >= 0) {
            executor.schedule(run, delay, TimeUnit.MILLISECONDS); 
            System.out.println("New notification was added in schedule");
        }
    }
}

package notificationservice.server;


import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/** Class stores a Notification object with <b>external_id</b>, <b>message</b>, 
 * <b>time</b>, <b>notification_type</b>, <b>extra_params</b> fields. 
*/
public class Notification {    
    private final String external_id;
    private final String message;
    private final Date time;
    private final NotificationType notification_type;
    private final String extra_params;
    /** Pattern for convert string to fields. */
    private static final Pattern pattern = 
            Pattern.compile("(.+),(.+),(.+),(.+),(.+)");      
    
    /** Create an object with given fields values.     
    */
    public Notification(String external_id, String message, Date time, 
            NotificationType notification_type, String extra_params) {
        this.external_id = external_id;
        this.message = message;
        this.time = time;
        this.notification_type = notification_type;
        this.extra_params = extra_params;        
    }    

    public String getExternal_id() {
        return external_id;
    }

    public String getMessage() {
        return message;
    }

    public Date getTime() {
        return time;
    }

    public NotificationType getNotification_type() {
        return notification_type;
    }

    public String getExtra_params() {
        return extra_params;
    }
    
    /** Static factory method for creation an object by given command. 
     * <b>pattern</b> field is used to parse given command to receive fields 
     * of notification.
     * @param str - given command
    */
    public static Notification getNotification(String str) {           
        Matcher m = pattern.matcher(str);
        if (m.matches()) {            
            Date time = ValueConverter.StringToDate(m.group(3));
            if (time != null) {
                NotificationType norification_type = 
                        NotificationType.getByString(m.group(4));
                if (norification_type != null) {
                    String external_id = m.group(1);
                    String message = m.group(2);
                    String extra_params = m.group(5);
                    return new Notification(external_id, message, time, 
                            norification_type, extra_params);
                }
            }
        }
        return null;
    }
}

package notificationservice.server;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/** Runnable for Notification object with NotificationType <b>http</b>.
*/
public class HTTPNotification implements Runnable {
    /** Address for connection to web server. */    
    private final String urlAddress;
    /** Property for http request */
    private final String message;
    /** Pattern for parse of extra_params field of Notification object. */
    private static final Pattern pattern = Pattern.compile("url=(.+)"); 
    
    /** Create an object with given notification. */
    HTTPNotification(Notification notification) {
        message = notification.getMessage();
        Matcher m = pattern.matcher(notification.getExtra_params());
        if (m.matches()) {
            urlAddress = m.group(1);
        } else {
            urlAddress = "";
        }
    }
    
    /** Send http request. In current version <b>GET</b> method of http 
     * protocol is used. <b>message</b> field of Notification object is used 
     * as <b>User-Agen</b> in http request.
     * @return result of http request.
    */
    public String getHTML() {
        try {
            StringBuilder result = new StringBuilder();
            URL url = new URL(this.urlAddress);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("User-Agent", message);
            try (BufferedReader rd = new BufferedReader(
                    new InputStreamReader(conn.getInputStream()))) {
                String line;
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }
            }
            return result.toString();
        } catch (Exception ex) {
            return ex.toString();
        }
    }
    
    /** Calls getHTML method and display result in console.
     * @see getHTML()
    */
    @Override
    public void run() {
        System.out.println(getHTML());
    }
}

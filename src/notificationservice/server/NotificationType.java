package notificationservice.server;


/** Enum of NotificationType.
 * @see Notification#notification_type
*/
public enum NotificationType { 
    mail, 
    http;
    
    /** Method for conversion string to NotificationType. */
    public static NotificationType getByString(String str) {
        switch (str) {
        case "mail":  
            return mail;
        case "http":  
            return http;    
        }
        return null;
    }
}

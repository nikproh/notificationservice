package notificationservice.server;


import java.io.InputStream;
import java.net.Socket;


/** Class processes a single command that was come to server.
 * @see TCPServer#start()
*/
public class ConnectionThread extends Thread {
    private final Socket socket;
    
    /** Create an object with given socket.     
    */
    ConnectionThread(Socket socket) {
        this.socket = socket;
    }
    
    /** Data from socket is read, notification object is received based on data,
     * runnable object is received based on notification object and this 
     * runnable object is send to executor.
     * @see Notification#getNotification(String str)
     * @see NotificationRunnable
     * @see NotificationExecutor
    */
    @Override
    public void run() {
        try (InputStream is = socket.getInputStream()) {
            byte buf[] = new byte[1024];
            int size = is.read(buf);                 
            String str = new String(buf, 0, size);
            Notification notification = Notification.getNotification(str);
            if (notification != null) {
                Runnable run = NotificationRunnable.get(notification);
                NotificationExecutor.addNotification(run, 
                        notification.getTime());            
            } else {
                System.out.println("Wrong command: " + str);
            }
        } catch (Exception e) {
            System.out.println("Connection socket error: " + e);
        } 
    }
}

package notificationservice.server;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/** Class for conversion types.
*/
public class ValueConverter {
    /** Pattern for conversion string to date. */
    private static final String pattern = "HH:mm dd.MM.yyyy";  
    
    /** Method for conversion string to date. 
     * <b>pattern</b> field is used to convert given string to date.
     * @param str - given string
     * @return result of conversion
    */
    static Date StringToDate(String str) {
        DateFormat format = new SimpleDateFormat(pattern, Locale.ENGLISH);
        try {
            Date date = format.parse(str);
            return date;
        } catch (ParseException ex) {
            return null;
        }        
    }
}
